#!/usr/bin/perl

use strict;
use warnings;

use Data::Dumper;

use WWW::Wikipedia;
use JSON::Any;

my $wiki = WWW::Wikipedia->new();

my %deaths_by_year;
my %deaths_by_yearmonth;

for my $year ( 2006 .. 2016 ){
	$deaths_by_year{ $year } = 0;
	$deaths_by_yearmonth{ $year } = {};
	for my $month ( 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'){
		my $article = "Deaths_in_${month}_${year}";
		my $result = $wiki->search( $article );
		if( $result->raw() ){
			my @entries = $result->raw() =~ m/(^\*\s*\[\[[^\]]+\]\])/gm;
			$deaths_by_year{ $year } += $#entries;
			$deaths_by_yearmonth{ $year }{ $month } = $#entries;
		}
	}
}

print JSON::Any->objToJson( \%deaths_by_year );
print JSON::Any->objToJson( \%deaths_by_yearmonth );
