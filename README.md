It's felt like 2016 had more celebrity deaths than ever. But did it?

Using the benchmark of "has a Wikipedia article", the script here counts how many notable deaths occurred each year (and month) since 2006.

The short answer is 'no', 2016 wasn't below the trend seen over the preceding 10 years.

To save you installing the required CPAN modules and running it, here's the results as JSON objects:

Notable deaths by year:

```
#!json
{"2016":6459,"2010":4307,"2008":3460,"2014":6359,"2007":2880,"2013":5853,"2006":2568,"2011":4312,"2009":3937,"2015":6270,"2012":5237}

```

Notable deaths by month:
```
#!json
{"2016":{"September":494,"May":520,"November":469,"October":510,"December":521,"February":549,"June":522,"July":538,"March":626,"January":667,"April":526,"August":517},"2010":{"September":323,"May":360,"November":289,"October":356,"December":327,"February":366,"June":373,"July":382,"March":380,"January":429,"April":382,"August":340},"2008":{"September":294,"May":300,"November":272,"October":305,"December":310,"February":299,"June":263,"July":250,"March":263,"January":365,"April":271,"August":268},"2014":{"September":506,"May":570,"November":494,"October":493,"December":518,"February":554,"June":495,"July":511,"March":537,"January":585,"April":548,"August":548},"2007":{"September":203,"May":204,"November":261,"October":261,"December":266,"February":231,"June":238,"July":249,"March":219,"January":266,"April":253,"August":229},"2013":{"September":416,"May":470,"November":447,"October":425,"December":497,"February":505,"June":502,"July":474,"March":513,"January":602,"April":528,"August":474},"2006":{"September":233,"May":218,"November":230,"October":228,"December":209,"February":205,"June":215,"July":236,"March":193,"January":199,"April":176,"August":226},"2011":{"September":307,"May":349,"November":387,"October":386,"December":387,"February":346,"June":358,"July":377,"March":339,"January":390,"April":349,"August":337},"2009":{"September":346,"May":297,"November":324,"October":338,"December":354,"February":294,"June":301,"July":301,"March":334,"January":434,"April":291,"August":323},"2015":{"September":498,"May":503,"November":491,"October":531,"December":473,"February":527,"June":519,"July":501,"March":583,"January":612,"April":572,"August":460},"2012":{"September":400,"May":415,"November":453,"October":410,"December":520,"February":419,"June":393,"July":407,"March":469,"January":531,"April":408,"August":412}}
```